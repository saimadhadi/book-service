package com.epam.bookservice.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.epam.bookservice.exception.DataNotFoundException;
import com.epam.bookservice.model.BookModel;
import com.epam.bookservice.service.BookService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

class BookControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Mock
	private BookService bookService;

	@InjectMocks
	private BookController bookController;

	private static ObjectMapper mapper;

	private static Long bookId = 1L;
	private static String bookName = "Abc";
	private static String bookAuthor = "Abc";
	private static String bookGenre = "Abc";
	private static String bookDescription = "Abc";
	private static BookModel bookModel;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(bookController).build();
	}

	@BeforeAll
	public static void init_data() {
		mapper = new ObjectMapper();
		bookModel = new BookModel(bookId, bookName, bookGenre, bookAuthor, bookDescription);
	}

	@Test
	public void getAllBooksReturnsBooksListAndStatusOk() throws Exception {
		List<BookModel> bookModelList = new ArrayList<>();
		bookModelList.add(bookModel);
		Mockito.when(bookService.getAllBooks()).thenReturn(bookModelList);
		this.mockMvc.perform(get("/books")).andExpect(content().json(mapper.writeValueAsString(bookModelList)))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON));
		Mockito.verify(bookService).getAllBooks();

	}

	@Test
	public void getAllBooksReturnBookListAndStatusOk() throws JsonProcessingException, Exception {
		Mockito.when(bookService.getBookById(1L)).thenReturn(bookModel);
		mockMvc.perform(get("/books/1")).andExpect(status().isOk())
				.andExpect(content().json(mapper.writeValueAsString(bookModel)))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON));
	}

	@Test
	public void getBookByIdReturnsBookAndStatusOk() throws Exception {
		Mockito.when(bookService.getBookById(bookId)).thenReturn(bookModel);
		this.mockMvc.perform(get("/books/1")).andExpect(content().json(mapper.writeValueAsString(bookModel)))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON));
		Mockito.verify(bookService).getBookById(bookId);

	}

	@Test
	public void getBookByIdReturnsStatusNotFound() throws Exception {
		Mockito.when(bookService.getBookById(bookId)).thenThrow(DataNotFoundException.class);
		this.mockMvc.perform(get("/books/1")).andExpect(status().is4xxClientError());
		Mockito.verify(bookService).getBookById(bookId);
	}

	@Test
	public void addBookReturnsAddedBookAndStatusCreated() throws Exception {
		Mockito.when(bookService.addBook(bookModel)).thenReturn(bookModel);
		this.mockMvc
				.perform(post("/books").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(bookModel)))
				.andExpect(content().json(mapper.writeValueAsString(bookModel))).andExpect(status().is2xxSuccessful())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON));
		Mockito.verify(bookService).addBook(bookModel);
	}

	@Test
	public void deleteBookByIdReturnsBookAndStatusOk() throws Exception {
		Mockito.when(bookService.deleteBookById(bookId)).thenReturn(bookModel);
		this.mockMvc.perform(delete("/books/1")).andExpect(content().json(mapper.writeValueAsString(bookModel)))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON));
		Mockito.verify(bookService).deleteBookById(bookId);

	}

	@Test
	public void deleteBookByIdReturnsStatusNotFound() throws Exception {
		Mockito.when(bookService.deleteBookById(bookId)).thenThrow(DataNotFoundException.class);
		this.mockMvc.perform(delete("/books/1")).andExpect(status().is4xxClientError());
		Mockito.verify(bookService).deleteBookById(bookId);
	}

	@Test
	public void updateBookReturnsBookAndStatusOk() throws Exception {
		Mockito.when(bookService.updateBook(bookModel)).thenReturn(bookModel);
		this.mockMvc
				.perform(put("/books/1").contentType(MediaType.APPLICATION_JSON)
						.content(mapper.writeValueAsString(bookModel)))
				.andExpect(content().json(mapper.writeValueAsString(bookModel))).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON));
		Mockito.verify(bookService).updateBook(bookModel);
	}

}
