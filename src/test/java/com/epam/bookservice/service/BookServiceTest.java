package com.epam.bookservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.epam.bookservice.entity.BookEntity;
import com.epam.bookservice.exception.DataNotFoundException;
import com.epam.bookservice.model.BookModel;
import com.epam.bookservice.repository.BookRepository;
import com.epam.bookservice.service.impl.BookServiceImpl;

@ExtendWith(SpringExtension.class)
public class BookServiceTest {

	@Mock
	private BookRepository bookRepository;

	@InjectMocks
	private BookServiceImpl bookService;

	private static Long bookId = 1L;
	private static String bookName = "Abc";
	private static String bookAuthor = "Abc";
	private static String bookGenre = "Abc";
	private static String bookDescription = "Abc";
	private static BookEntity bookEntity;
	private static BookModel bookModel;

	@BeforeAll
	public static void init() {
		bookEntity = new BookEntity(bookId, bookName, bookGenre, bookAuthor, bookDescription);
		bookModel = new BookModel(bookId, bookName, bookGenre, bookAuthor, bookDescription);
	}

	@Test
	public void getAllBooksReturnsBookList() {
		when(bookRepository.findAll()).thenReturn(Arrays.asList(bookEntity));
		List<BookModel> expected = Arrays.asList(bookModel);
		assertEquals(expected, bookService.getAllBooks());
		Mockito.verify(bookRepository).findAll();
	}

	@Test
	public void getBookByIdReturnsBook() {
		when(bookRepository.findById(bookId)).thenReturn(Optional.ofNullable(bookEntity));
		assertEquals(bookModel, bookService.getBookById(bookId));
		Mockito.verify(bookRepository).findById(bookId);
	}

	@Test
	public void getBookByIdThrowsDataNotFoundException() {
		when(bookRepository.findById(bookId)).thenReturn(Optional.ofNullable(null));
		assertThrows(DataNotFoundException.class, () -> bookService.getBookById(bookId));
		Mockito.verify(bookRepository).findById(bookId);
	}

	@Test
	public void addBookReturnsAddedBook() {
		when(bookRepository.save(Mockito.any())).thenReturn(bookEntity);
		assertEquals(bookModel, bookService.addBook(bookModel));
		Mockito.verify(bookRepository).save(Mockito.any());
	}

	@Test
	public void updateBookReturnsUpdatedBook() {
		when(bookRepository.save(Mockito.any())).thenReturn(bookEntity);
		assertEquals(bookModel, bookService.updateBook(bookModel));
		Mockito.verify(bookRepository).save(Mockito.any());
	}

	@Test
	public void deleteBookByIdReturnsDeletedBook() {
		when(bookRepository.findById(bookId)).thenReturn(Optional.ofNullable(bookEntity));
		Mockito.doNothing().when(bookRepository).delete(bookEntity);
		assertEquals(bookModel, bookService.deleteBookById(bookId));
		Mockito.verify(bookRepository).findById(bookId);
		Mockito.verify(bookRepository).delete(bookEntity);
	}

	@Test
	public void deleteBookByIdThrowsDataNotFoundException() {
		when(bookRepository.findById(bookId)).thenReturn(Optional.ofNullable(null));
		assertThrows(DataNotFoundException.class, () -> bookService.deleteBookById(bookId));
		Mockito.verify(bookRepository).findById(bookId);
	}

}
