create table if not exists book (
	book_id bigint auto_increment,
	name varchar(255),
	author varchar(255),
	description varchar(255),
	genre varchar(255),
	primary key(book_id)
);