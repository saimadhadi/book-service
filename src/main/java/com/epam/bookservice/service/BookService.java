package com.epam.bookservice.service;

import java.util.List;

import com.epam.bookservice.model.BookModel;

public interface BookService {
	List<BookModel> getAllBooks();

	BookModel getBookById(Long bookId);

	BookModel addBook(BookModel bookModel);

	BookModel deleteBookById(Long bookId);

	BookModel updateBook(BookModel bookModel);
}
