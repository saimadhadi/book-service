package com.epam.bookservice.service.impl;

import static java.util.stream.Collectors.toList;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.bookservice.entity.BookEntity;
import com.epam.bookservice.exception.DataNotFoundException;
import com.epam.bookservice.model.BookModel;
import com.epam.bookservice.repository.BookRepository;
import com.epam.bookservice.service.BookService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service("com.epam.bookservice.service.impl.BookServiceImpl")
public class BookServiceImpl implements BookService {

	@Autowired
	private BookRepository bookRepository;

	private static final Logger log = LogManager.getLogger(BookServiceImpl.class);
	private static final ObjectMapper mapper = new ObjectMapper();

	@Override
	public List<BookModel> getAllBooks() {
		List<BookModel> bookModelList = bookRepository.findAll().stream().map(BookModel::new).collect(toList());
		log.info("Fetched book list : {}", bookModelList);
		return bookModelList;
	}

	@Override
	public BookModel getBookById(Long bookId) {
		BookEntity bookEntity = bookRepository.findById(bookId)
				.orElseThrow(() -> new DataNotFoundException("Book not found"));
		BookModel bookModel = mapper.convertValue(bookEntity, BookModel.class);
		log.info("Fetched Book with id : {}, {}", bookId, bookEntity);
		return bookModel;
	}

	@Override
	public BookModel addBook(BookModel bookModel) {
		BookEntity bookEntity = mapper.convertValue(bookModel, BookEntity.class);
		bookEntity = bookRepository.save(bookEntity);
		BookModel bookModelAdded = mapper.convertValue(bookEntity, BookModel.class);
		log.info("Added book : {} with id : {}", bookModel, bookModelAdded.getBookId());
		return bookModelAdded;
	}

	@Override
	public BookModel deleteBookById(Long bookId) {
		BookEntity bookEntity = bookRepository.findById(bookId)
				.orElseThrow(() -> new DataNotFoundException("Book not found"));
		bookRepository.delete(bookEntity);
		BookModel bookModel = mapper.convertValue(bookEntity, BookModel.class);
		log.info("Book with id : {} has been deleted", bookId);
		return bookModel;
	}

	@Override
	public BookModel updateBook(BookModel bookModel) {
		BookEntity bookEntity = mapper.convertValue(bookModel, BookEntity.class);
		bookEntity = bookRepository.save(bookEntity);
		bookModel = mapper.convertValue(bookEntity, BookModel.class);
		log.info("Book with has been updated : {}", bookModel);
		return bookModel;
	}
}
