package com.epam.bookservice.exception;

public class DataNotFoundException extends RuntimeException {

   public DataNotFoundException(String message) {
      super(message);
   }
}
