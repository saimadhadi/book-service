package com.epam.bookservice.exception;

public class ServerException extends RuntimeException {

   public ServerException(String message) {
      super(message);
   }
}
