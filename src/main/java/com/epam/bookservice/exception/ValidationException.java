package com.epam.bookservice.exception;

public class ValidationException extends RuntimeException {
   public ValidationException(String message) {
      super(message);
   }
}
