package com.epam.bookservice.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.epam.bookservice.exception.DataNotFoundException;
import com.epam.bookservice.model.BookModel;
import com.epam.bookservice.service.BookService;

@RestController
public class BookController {

	@Autowired
	@Qualifier("com.epam.bookservice.service.impl.BookServiceImpl")
	private BookService bookService;

	private static final Logger log = LogManager.getLogger(BookController.class);

	@GetMapping("/books")
	public ResponseEntity<List<BookModel>> getAllBooks() {
		List<BookModel> bookModelList = bookService.getAllBooks();
		log.info("Fethced all books : {}", bookModelList);
		return new ResponseEntity<>(bookModelList, HttpStatus.OK);
	}

	@GetMapping("/books/{bookId}")
	public ResponseEntity<BookModel> getBookById(@PathVariable Long bookId) {
		ResponseEntity<BookModel> responseEntity;
		try {
			BookModel bookModel = bookService.getBookById(bookId);
			responseEntity = new ResponseEntity<>(bookModel, HttpStatus.OK);
			log.info("Fetched book : {}", bookModel);
		} catch (DataNotFoundException e) {
			log.debug("Book with id : {} is not present", bookId, e);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Book not present", e);
		}
		return responseEntity;
	}

	@PostMapping("/books")
	public ResponseEntity<BookModel> addBook(@RequestBody BookModel bookModel) {
		BookModel bookModelAdded = bookService.addBook(bookModel);
		log.info("Added Book : {}", bookModel);
		return new ResponseEntity<>(bookModelAdded, HttpStatus.CREATED);
	}

	@PutMapping("/books/{bookId}")
	public ResponseEntity<BookModel> updateBook(@RequestBody BookModel bookModel) {
		BookModel bookModelUpdated = bookService.updateBook(bookModel);
		log.info("Updated Book : {}", bookModel);
		return new ResponseEntity<>(bookModelUpdated, HttpStatus.OK);
	}

	@DeleteMapping("books/{bookId}")
	public ResponseEntity<BookModel> deleteBook(@PathVariable Long bookId) {
		ResponseEntity<BookModel> responseEntity;
		try {
			BookModel bookModelDeleted = bookService.deleteBookById(bookId);
			responseEntity = new ResponseEntity<>(bookModelDeleted, HttpStatus.OK);
			log.info("Deleted Book with id : {}, {}", bookId, bookModelDeleted);
		} catch (DataNotFoundException e) {
			log.debug("Book with id : {} not present", bookId, e);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Book not present", e);
		}
		return responseEntity;
	}
}
