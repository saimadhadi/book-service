package com.epam.bookservice.repository;

import com.epam.bookservice.entity.BookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("com.epam.bookservice.repository.BookRepository")
public interface BookRepository extends JpaRepository<BookEntity,Long> {
}
