package com.epam.bookservice.model;

import com.epam.bookservice.entity.BookEntity;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class BookModel {

	private Long bookId;
	private String bookName;
	private String bookGenre;
	private String bookAuthor;
	private String bookDescription;

	public BookModel(BookEntity bookEntity) {
		this.bookId = bookEntity.getBookId();
		this.bookName = bookEntity.getBookName();
		this.bookGenre = bookEntity.getBookGenre();
		this.bookAuthor = bookEntity.getBookAuthor();
		this.bookDescription = bookEntity.getBookDescription();
	}

}
